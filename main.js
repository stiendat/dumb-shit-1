//osu related
//config
var nodesu = require("nodesu");

var api = new nodesu.Client("*api.key*", {parseData : true});

//stuff

function Parse(json) {
    var data = JSON.parse(json);
    //msg.channel.send(data.playcount, "pc ", data.ppraw , "pp  #", data.ppCountryRank, " vn");
}

//discord bot
var discord = require("discord.js");
var bot = new discord.Client();
bot.login("discord.bot.api");

bot.on('ready', () => {
    console.log("logged in as ", bot.user.tag);
});

//discord commands
commands = {
    "test": {
        process: function(bot, msg, suffix) {
            msg.reply("ok1");
        }
    },

    "getinfo": {
        process: function(bot, msg, suffix) {
            var json = api.user
                .get(suffix, String)
                .then(function(fulfilled) {
                    var myObj = fulfilled;
                    data = {
                        "username": {
                            content: myObj["username"]
                        },
                        "playcount": {
                            content: myObj["playcount"]
                        },
                        "pp": {
                            content: myObj["pp"]
                        },
                        "country": {
                            content: myObj["country"]
                        },
                        "rank": {
                            content: myObj["ppRank"]
                        },
                        "countryrank": {
                            content: myObj["ppCountryRank"]
                        }
                    };
                    console.log("User:", data.username.content, " Playcount:", data.playcount.content, " PP:", data.pp.content, " Country:", data.country.content, " Rank:", data.rank.content, " Country Rank:", data.countryrank.content);
                    
                    
                });
        }
    }
};

//event handler
bot.on('message', msg => {
    if (msg.content === "test") {
        msg.reply("ok");
    };
    if (msg.author.id !== bot.user.id && msg.content.startsWith("?"))   {
        var cmdText1 = msg.content.split(" ")[0];
        var cmdText = cmdText1.slice(1);
        var suffix = msg.content.split(" ")[1];
        var cmd = commands[cmdText];
        //msg.channel.send(suffix);
        cmd.process(bot, msg, suffix);
    } 

    
});